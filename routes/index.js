var express = require("express");
var router = express.Router();
const excel = require('exceljs');
const app = require("../app");

/* GET home page. */
router.get("/", function (req, res, next) {
  res.redirect("/pegawai/");
});

module.exports = router;
